import * as path from 'path';
import { tap, expect } from '@pushrocks/tapbundle';
import * as qenv from '../ts/index.js';

import * as smartpath from '@pushrocks/smartpath';

export { smartpath };

const testDir = smartpath.get.dirnameFromImportMetaUrl(import.meta.url);

process.env['key1'] = 'original';

let testQenv: qenv.Qenv;

tap.test('should create a new class', async () => {
  testQenv = new qenv.Qenv(path.join(testDir, 'assets'), path.join(testDir, 'assets'), false);
  expect(testQenv).toBeInstanceOf(qenv.Qenv);
});

tap.test('key1 should be not be overwritten since it is already present', async () => {
  expect(testQenv.getEnvVarRequired('key1')).toEqual('original');
  expect(testQenv.getEnvVarOnDemand('key1')).toEqual('original');
});

tap.test('key2 should be read from Yml', async () => {
  expect(testQenv.getEnvVarRequired('key2')).toEqual('fromJson');
  expect(testQenv.getEnvVarOnDemand('key2')).toEqual('fromJson');
});

tap.test('keyValueObjectArray should hold all retrieved values', async () => {
  expect(testQenv.keyValueObject.key1).toEqual('original');
  expect(testQenv.keyValueObject.key2).toEqual('fromJson');
});

tap.start();
